<?php
/**
* @package Pet Memorial
* @version 1.0
*/
/*
Plugin Name: Pet Memorial Custom Post
Plugin URI: http://www.ripplewerkz.com
Description: This Plugin is for Memories of Life - Pet Memorial Custom Post
Author: Hercival Aragon
Version: 1.0
*/
add_action('init', 'petmemorial_register');
add_action('admin_init', 'admin_petmemorial_init');
define( 'PETMENORIAL_PATH', plugin_dir_path( __FILE__ ) );
function petmemorial_register() {
 
	$labels = array(
		'name' => _x('Pet Memorials', 'Pet Memorial List Item'),
		'singular_name' => _x('Pet Memorial Item', 'Pet Memorial Item'),
		'add_new' => _x('Add Pet Memorial', 'Pet Memorial item'),
		'add_new_item' => __('Add New Pet Memorial Item'),
		'edit_item' => __('Edit Pet Memorial Item'),
		'new_item' => __('New Pet Memorial Item'),
		'view_item' => __('View Pet Memorial Item'),
		'search_items' => __('Search Obituary'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);
 
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'menu_icon' => 'dashicons-id-alt',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 2,
		'supports' => array('title','editor','thumbnail','comments'),
        'has_archive'   => true,
        'rewrite' => array('slug' => 'petmemorial','with_front' => true),
	  ); 
 
	register_post_type( 'petmemorial' , $args );
}





function admin_petmemorial_init(){
    add_meta_box("age-died-meta", "Age", "pet_age_died", "petmemorial", "side", "low");
    add_meta_box("pet-type-meta", "Kind of Pet", "pet_type", "petmemorial", "side", "low");
    add_meta_box("from-date-meta", "Date of Birth", "petmemorial_date", "petmemorial", "side", "low", array('nameid' => 'pet_from'));
    add_meta_box("to-date-meta", "Date of Death", "petmemorial_date", "petmemorial", "side", "low",  array('nameid' => 'pet_to'));
    add_meta_box("gmap-place-id", "Location Google Place ID", "pet_gmap_place_id_func", "petmemorial");
    add_meta_box("wake-address", "Wake Address", "pet_wake_address", "petmemorial", "normal", "low");
    add_meta_box("funeral-details", "Funeral Details", "pet_funeral_details", "petmemorial", "normal", "low");
    add_meta_box("funeral-service", "Funeral Service", "funeral_service_func", "petmemorial", "side", "low");
    add_meta_box("template-id", "Template Profile", "pet_template_id_func", "petmemorial");
}
function pet_template_id_func(){
    global $post;
    $custom = get_post_custom($post->ID);
    $template_id = $custom["template_id"][0];
}
function pet_age_died(){
    global $post;
    $custom = get_post_custom($post->ID);
    $age_died = $custom["age_died"][0];
    echo '<h3>'.$age_died.'</h3>';
}

function pet_type(){
    global $post;
    $custom = get_post_custom($post->ID);
    $pet_type = $custom["pet_type"][0];
    echo '<input name="pet_type" value="'.$pet_type.'">';
}
function pet_wake_address(){
    global $post;
    $custom = get_post_custom($post->ID);
    $wake_address = $custom["wake_address"][0];
    $editor_id = 'wake_address';
    wp_editor( $wake_address, $editor_id );
}
function pet_funeral_details(){
    global $post;
    $custom = get_post_custom($post->ID);
    $funeral_details = $custom["funeral_details"][0];
    $editor_id = 'funeral_details';
    wp_editor( $funeral_details, $editor_id );
}

function pet_gmap_place_id_func(){
    global $post;
    $custom = get_post_custom($post->ID);
    $place_id = $custom["gmap_place_id"][0];
    //echo '<input name="gmap_place_id" value="'.$place_id.'">';
}
function pet_funeral_service_func(){
    global $post;
    $custom = get_post_custom($post->ID);
    $funeral_service_val = $custom["funeral_service"][0];
    $args = array(
        'post_type'   => 'funeralservice',
        'post_status' => 'publish'
    );
    $returnhtml = '';
    $funeralservice = get_posts( $args );
    echo '<select name="funeral_service" class="form-control ob-input">';
    
    if( $funeralservice ) :
        
        foreach( $funeralservice as $funeralservice_post ) : 
            echo '<option value="'.$funeralservice_post->ID.'" '.($funeral_service_val == $funeralservice_post->ID ? 'selected' : '').'>'.$funeralservice_post->post_title.'</option>';
        endforeach;
        
        wp_reset_postdata();
    endif;
    echo '</select>';
}
//function get_funeral_service_list($selected = 0){
//    $args = array(
//        'post_type'   => 'funeralservice',
//        'post_status' => 'publish'
//    );
//    $returnhtml = '';
//    $funeralservice = new WP_Query( $args );
//    if( $funeralservice->have_posts() ) :
//        $returnhtml = '<select name="funeral_service" class="form-control ob-input">';
//        while( $funeralservice->have_posts() ) :
//            $funeralservice->the_post();
//            $returnhtml .= '<option value="'.get_the_ID().'" '.($selected == get_the_ID() ? 'selected' : '').'>'.get_the_title().'</option>';
//        endwhile;
//        $returnhtml .= '</select>';
//        wp_reset_postdata();
//    endif;
//    
//    return $returnhtml;
//
// }

function petmemorial_date($post, $args) {
    $metabox_id = $args['args']['nameid'];
    global $post, $wp_locale;
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'petmemorial_posts_nonce' );
    $time_adj = current_time( 'timestamp' );
    $pet_date = intval(get_post_meta( $post->ID, $metabox_id . '_date', true ));
    $month = gmdate('m', $pet_date);
    $day = gmdate( 'd', $pet_date );
    $year = gmdate( 'Y', $pet_date );
    $hour = gmdate( 'H', $pet_date );
    $min = gmdate( 'i', $pet_date );

    $month_s = '<select name="' . $metabox_id . '_month">';
    for ( $i = 1; $i < 13; $i = $i +1 ) {
        $month_s .= "\t\t\t" . '<option value="' . zeroise( $i, 2 ) . '"';
        if ( $i == $month )
            $month_s .= ' selected="selected"';
        $month_s .= '>' . $wp_locale->get_month_abbrev( $wp_locale->get_month( $i ) ) . "</option>\n";
    }
    $month_s .= '</select>';
    echo $month_s;
    echo '<input type="text" name="' . $metabox_id . '_day" value="' . $day  . '" size="2" maxlength="2" />';
    echo '<input type="text" name="' . $metabox_id . '_year" value="' . $year . '" size="4" maxlength="4" /> @ ';
    echo '<input type="hidden" name="' . $metabox_id . '_hour" value="0" size="2" maxlength="2"/>:';
    echo '<input type="hidden" name="' . $metabox_id . '_minute" value="0" size="2" maxlength="2" />';
}

add_action('save_post', 'pet_save_details');

function pet_save_details(){
    global $post;
    //date from preparation
    $pet_from_month = $_POST['pet_from_month'];
    $pet_from_day = $_POST['pet_from_day'];
    $pet_from_year = $_POST['pet_from_year'];
    $pet_from_hour = $_POST['pet_from_hour'];
    $pet_from_minute = $_POST['pet_from_minute'];
    $dateform = strtotime($pet_from_year.'-'.$pet_from_month.'-'.$pet_from_day.' '.$pet_from_hour.':'.$pet_from_minute);
    //date to preparation
    $pet_to_month = $_POST['pet_to_month'];
    $pet_to_day = $_POST['pet_to_day'];
    $pet_to_year = $_POST['pet_to_year'];
    $pet_to_hour = $_POST['pet_to_hour'];
    $pet_to_minute = $_POST['pet_to_minute'];
    $datato = strtotime($pet_to_year.'-'.$pet_to_month.'-'.$pet_to_day.' '.$pet_to_hour.':'.$pet_to_minute);
    
    update_post_meta($post->ID, "pet_type", $_POST["pet_type"]);
    update_post_meta($post->ID, "pet_from_date", $dateform);
    update_post_meta($post->ID, "pet_to_date", $datato);
    update_post_meta($post->ID, "wake_address", $_POST["wake_address"]);
    update_post_meta($post->ID, "funeral_details", $_POST["funeral_details"]);
    update_post_meta($post->ID, "funeral_service", $_POST["funeral_service"]);
     update_post_meta($post->ID, "template_id", $_POST["template_id"]);
}


//Adding Columns To the list of orbituary
add_action("manage_posts_custom_column",  "petmemorial_custom_columns");
add_filter("manage_edit-petmemorial_columns", "petmemorial_edit_columns");

function petmemorial_edit_columns($columns){
  $columns = array(
    "cb" => '<input type="checkbox" />',
    "title" => "Full Name",
    "description" => "Details",
    "age_died" => "Age Died",
    "pet_type" => "Pet Type",
    "post_date" => "Date",
    "status" => "Status"
  );
 
  return $columns;
}
function petmemorial_custom_columns($column){
  global $post;
    $custom = get_post_custom();
    switch ($column) {
    case "description":
        the_excerpt();
        break;
    case "age_died":
        echo $custom["age_died"][0];
            break;
    case "pet_type":
        echo $custom["pet_type"][0];
        break;
    case "post_date":
        echo the_date();
        break;
    //case "status": echo get_post_status(); break;
    }
}


class New_Petmemorial_Widget extends WP_Widget {
    public function __construct() { 
        $widget_options = array( 'classname' => 'petmemorial_widget', 'description' => 'This is a New Pet Memorial Form', ); 
        parent::__construct( 'petmemorial_widget', 'Pet Memorial Form Widget', $widget_options ); 
    }
    
    public function widget( $args, $instance ) {
        
        if($_POST){
            $fullname = $_POST['petname'];
            $post_information = array(
                'post_title' => wp_strip_all_tags( $fullname ),
                'post_content' => $_POST['description'],
                'post_type' => 'petmemorial',
                'post_status' => 'pending'
            );
            $pet_from_date = str_replace('/', '-', $_POST['pet_from_date']);
            $pet_from_date = strtotime($pet_from_date);
            $pet_to_date = str_replace('/', '-', $_POST['pet_to_date']);
            $pet_to_date = strtotime($pet_to_date);
            $age = date('Y', $pet_to_date) - date('Y', $pet_from_date);
            $post_id = wp_insert_post( $post_information );
            
            if($post_id){
                add_post_meta($post_id, 'pet_type', wp_strip_all_tags( $_POST['pet_type'] ), true);
                add_post_meta($post_id, 'pet_from_date', $pet_from_date, true);
                add_post_meta($post_id, 'pet_to_date',$pet_to_date, true);
                add_post_meta($post_id, 'age_died', $age , true);
                add_post_meta($post_id, "wake_address", wp_strip_all_tags($_POST["wake_address"]));
                add_post_meta($post_id, "funeral_details", wp_strip_all_tags($_POST["funeral_details"]));
                add_post_meta($post_id, "funeral_service",wp_strip_all_tags( $_POST["funeral_service"]));
                add_post_meta($post_id, "gmap_place_id",wp_strip_all_tags( $_POST["gmap_place_id"]));
    
                    require_once( ABSPATH . 'wp-admin/includes/post.php' );
                    require_once( ABSPATH . 'wp-admin/includes/image.php' );
                    require_once( ABSPATH . 'wp-admin/includes/file.php' );
                    require_once( ABSPATH . 'wp-admin/includes/media.php' );


                    $attachment_id = media_handle_upload( 'photo', $post_id );
                    
                    if ( is_wp_error( $attachment_id ) ) {
                        echo '<div class="alert alert-warning" role="alert">
                            <strong>Warning!</strong> There was a problem uploading your Photo.
                        </div>';
                    } else {
                        set_post_thumbnail( $post_id, $attachment_id );
                    }

                echo '<div class="alert alert-info" role="alert">
                        <strong>Well done!</strong> You successfully read this important alert message.
                    </div>';
            }else{
                echo '<div class="alert alert-warning" role="alert">
                        <strong>Warning!</strong> There was a problem. read this important alert message.
                    </div>';
            }
        }
        $title = apply_filters( 'widget_title', $instance['title'] );
        echo '<form action="" method="post" onkeypress="return event.keyCode != 13;" enctype="multipart/form-data" id="new-ob-form">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="">Pet Name *</label>
                                <input class="form-control ob-input validate[required]" type="text" name="petname" value="" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="">Pet Type *</label>
                                <input class="form-control ob-input validate[required]" type="text" name="pet_type" value="" placeholder="Example : Dog, Cat, Bird, Fish">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="">Date of Birth *</label>
                                <input class="datepicker form-control ob-input validate[required]" type="text" name="pet_from_date" value="" placeholder="DD/MM/YYYY">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="">Date of Death *</label>
                                <input class="datepicker form-control ob-input validate[required]" type="text" name="pet_to_date" value="" placeholder="DD/MM/YYYY">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="">Photo</label>
                                <div class="well center-block">
                                    <img src="'.plugins_url( 'images/user_default.jpg', __FILE__ ).'" id="image-preview" class="img-responsive img-thumbnail" style="margin-bottom: 15px;">
                                    <input type="file" name="photo" id="obituary-photo-input" class="form-control validate[required]">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="">Description *</label>
                                <textarea class="form-control ob-input validate[required]" name="description" id="description" cols="0" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="">Wake Address *</label>
                                <textarea class="form-control ob-input" name="wake_address" id="wake_address" cols="0" rows="3"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="">Funeral Details *</label>
                                <textarea class="form-control ob-input" name="funeral_details" id="funeral_details" cols="0" rows="3"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="">Funeral Service *</label>
                                '.get_funeral_service_list().'
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="">Location</label>
                                <input id="searchbox-gmap" class="form-control validate[required]" type="text" placeholder="Search Box" style="width:200px;top:8px !important;">
                                <div id="map" class="embed-responsive embed-responsive-4by3" style="height:500px;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="hidden" id="template_id" name="template_id" value="">  
                                <input type="hidden" id="gmap-place-id" name="gmap_place_id" value="">  
                                <input type="submit" id="submit-button" class="btn btn-info" value="SUBMIT">
                            </div>
                        </div>
                    </div>
                </form>';
}

}

function petmemorial_load_widget() {
    register_widget( 'New_Petmemorial_Widget' );

}
add_action( 'widgets_init', 'petmemorial_load_widget' );
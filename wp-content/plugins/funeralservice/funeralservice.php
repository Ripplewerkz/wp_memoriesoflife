<?php
/**
* @package funeral Service
* @version 1.0
*/
/*
Plugin Name: Funeral Service Custom Post
Plugin URI: http://www.ripplewerkz.com
Description: This Plugin is for Memories of Life - Funeral Service Custom Post
Author: Hercival Aragon
Version: 1.0
*/
add_action('init', 'funeralservice_register');
define( 'FS_PATH', plugin_dir_path( __FILE__ ) );
function funeralservice_register() {
 
	$labels = array(
		'name' => _x('Funeral Service List', 'Funeral Service Item'),
		'singular_name' => _x('Funeral Service Item', 'Funeral Service Item'),
		'add_new' => _x('Add Funeral Service', 'Funeral Service item'),
		'add_new_item' => __('Add New Funeral Service Item'),
		'edit_item' => __('Edit Funeral Service Item'),
		'new_item' => __('New Funeral Service Item'),
		'view_item' => __('View Funeral Service Item'),
		'search_items' => __('Search Funeral Service'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);
 
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'menu_icon' => 'dashicons-store',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 2,
		'supports' => array('title','editor','thumbnail'),
        'has_archive'   => true,
        'rewrite' => array('slug' => 'funeral-service','with_front' => true),
	  ); 
 
	register_post_type( 'funeralservice' , $args );
}


//Adding Columns To the list of Funeral Service

add_action("manage_posts_custom_column",  "funeralservice_custom_columns",  10, 2);
add_filter("manage_edit-funeralservice_columns", "funeralservice_edit_columns");

function funeralservice_edit_columns($columns){
    $columns = array(
        "cb" => '<input type="checkbox" />',
        'featured_image' => 'Image',
        "title" => "Funeral Service Name",
        "description" => "Details",
        "post_date" => "Date Created",
        "status" => "Status"
    );
    return $columns;
}

function funeralservice_custom_columns($column){
  global $post;
 
    switch ($column) {
        case 'featured_image':
            echo the_post_thumbnail( 'thumbnail' );
            break;
        case "post_date":
            echo the_date();
            break;
    }
}
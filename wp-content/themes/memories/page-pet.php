<?php 
/* Template Name: Pet Memorial Page */
get_header(); ?>
<header>
    <div class="header-content">
        <div class="auto med2"> 
            <div class="logo fl bp-img"><a href="index.html"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/page_template/icon_memories_of_life.png" alt=""></a></div>
            <div class="he-co-col right fr">
                <div class="hc-set menu">
                    <?php 
                        $args = array(
                            'theme_location' => 'petmenu',
                            'container'       => false,
                            'menu_class' => ''
                        );
                        wp_nav_menu( $args );
                    ?>
                </div>
                <!--div class="hc-set hc-ctrl">
                    <ul>
                        <li><a href="#"><span class="fi flaticon-camera"></span></a></li>
                        <li><a class="hc-set-search"><span class="fi flaticon-musica-searcher"></span></a></li>
                        <li><a href="#"><span class="fi flaticon-shopping-bag"></span></a></li>
                    </ul>
                </div-->
                <div class="hc-set menu-bar"><span></span><span></span><span></span></div>
            </div>
            <div class="clr"></div>
        </div>
    </div>
</header>

<!-- for floating menu -->
<div class="he-co-menu">
    <div class="auto med2 bp-rel">
        <div class="hc-me-close"><i class="fa fa-close"></i></div>
        <div class="hc-me-top">
            <div class="hc-ctrl">
                <ul>
                    <li><a href="#"><span class="fi flaticon-camera"></span></a></li>
                    <li><a href="#"><span class="fi flaticon-musica-searcher"></span></a></li>
                    <li><a href="#"><span class="fi flaticon-shopping-bag"></span></a></li>
                </ul>
            </div>
        </div>
        <div class="menu">
            <?php 
                $args = array(
                    'theme_location' => 'petmenu',
                    'container'       => false,
                    'menu_class' => ''
                );
                wp_nav_menu( $args );
            ?>
        </div>
    </div>
</div>
<!-- for floating menu -->
<div class="container-fluid he-co-floating hc-fl-search" style="display: none;">
    <div class="auto bp-rel">
        <div class="hc-me-close"><i class="fa fa-close"></i></div>
        <div class="hf-se-main" style="margin-top: 129px;">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input class="form-control" type="text" placeholder="Search...">
            </div>
        </div>
    </div>
</div>
<!-- Start of main content -->
<section id="main-wrapper">
		<div class="inner-content obituary-content pad-top">
			<div id="memories-parallax" class="pet-memorial-banner">
				<div class="pe-me-main">
					<div class="pe-me-object bp-rel">
						<div class="pm-ma-item group-pets layer" data-depth="0.07"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/obituary/group_pets.png" alt=""></div>
						<div class="pm-ma-item ropephotos layer" data-depth="0.1"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/obituary/rope_photos.png" alt=""></div>
						<div class="pm-ma-item rainbow layer" data-depth="0.02"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/obituary/bg_rainbow.png" alt=""></div>
						<div class="pm-ma-item clouds one layer" data-depth="0.13"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/obituary/bg_clouds_three.png" alt=""></div>
						<div class="pm-ma-item clouds two layer" data-depth="0.18"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/obituary/bg_clouds_four.png" alt=""></div>
						<div class="pm-ma-item clouds three layer" data-depth="0.14"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/obituary/bg_clouds_five.png" alt=""></div>
						<div class="pm-ma-item clouds four layer" data-depth="0.11"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/obituary/bg_clouds_six.png" alt=""></div>
						<div class="pm-ma-item sun-light one layer" data-depth="0.2"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/obituary/bg_sunlight2.png" alt=""></div>
						<div class="pm-ma-item sun-light two layer" data-depth="0.19"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/obituary/bg_sunlight3.png" alt=""></div>
					</div>
				</div>
			</div>
			<div class="container-fluid pb5">
				<div class="auto pb1">
					<a class="pe-me-btn top fr bp-set" href="<?php echo get_site_url(); ?>/petmemorial"><span class="icon"><i class="fa fa-angle-right"></i></span> <span><small>see memories of</small>your favorite pet</span></a>
					<div class="clr"></div>
				</div>
			</div>
		</div>
	</section>
<?php get_footer(); ?>
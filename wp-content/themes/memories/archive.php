<?php get_header(); ?>

<?php
    $args = array(
      'post_type' => 'obituary',
      'tax_query' => array(
        array(
          'taxonomy' => 'obituary_category',
          'field' => 'slug',
          'terms' => 'love-one'
        )
      )
    );
    $products = new WP_Query( $args );
    if( $products->have_posts() ) {
      while( $products->have_posts() ) {
        $products->the_post();
        ?>
          <h1><?php the_title() ?></h1>
          <div class='content'>
            <?php the_content() ?>
          </div>
        <?php
      }
    }
    else {
      echo 'Oh ohm no products!';
    }
  ?>


<?php get_footer(); ?>
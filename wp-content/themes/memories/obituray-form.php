<?php
/* Template Name: Obituray Form */
get_header(); ?>
<header>
    <div class="header-content">
        <div class="auto med2"> 
            <div class="logo fl bp-img"><a href="index.html"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/page_template/icon_memories_of_life.png" alt=""></a></div>
            <div class="he-co-col right fr">
                <div class="hc-set menu">
                    <?php 
                        $args = array(
                            'theme_location' => 'primary',
                            'container'       => false,
                            'menu_class' => ''
                        );
                        wp_nav_menu( $args );
                    ?>
                </div>
                <!--div class="hc-set hc-ctrl">
                    <ul>
                        <li><a href="#"><span class="fi flaticon-camera"></span></a></li>
                        <li><a class="hc-set-search"><span class="fi flaticon-musica-searcher"></span></a></li>
                        <li><a href="#"><span class="fi flaticon-shopping-bag"></span></a></li>
                    </ul>
                </div-->
                <div class="hc-set menu-bar"><span></span><span></span><span></span></div>
            </div>
            <div class="clr"></div>
        </div>
    </div>
</header>

<!-- for floating menu -->
<div class="he-co-menu">
    <div class="auto med2 bp-rel">
        <div class="hc-me-close"><i class="fa fa-close"></i></div>
        <div class="hc-me-top">
            <div class="hc-ctrl">
                <ul>
                    <li><a href="#"><span class="fi flaticon-camera"></span></a></li>
                    <li><a href="#"><span class="fi flaticon-musica-searcher"></span></a></li>
                    <li><a href="#"><span class="fi flaticon-shopping-bag"></span></a></li>
                </ul>
            </div>
        </div>
        <div class="menu">
            <?php 
                $args = array(
                    'theme_location' => 'primary',
                    'container'       => false,
                    'menu_class' => ''
                );
                wp_nav_menu( $args );
            ?>
        </div>
    </div>
</div>
<!-- for floating menu -->
<div class="container-fluid he-co-floating hc-fl-search" style="display: none;">
    <div class="auto bp-rel">
        <div class="hc-me-close"><i class="fa fa-close"></i></div>
        <div class="hf-se-main" style="margin-top: 129px;">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input class="form-control" type="text" placeholder="Search...">
            </div>
        </div>
    </div>
</div>
<!-- Start of main content -->
	<section id="main-wrapper">
		<div class="in-fl-co right">
			<a class="bp-img" href="<?php echo get_site_url(); ?>/post-new-obituary/"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/obituary/btn_post_new.png" alt=""></a>
		</div>
		<div class="inner-content obituary-content pad-top">
			<div class="container-fluid pt5 pb3 animate">
				<div class="auto med pt2">
					<h1 class="hb-title text-center bp-tt">post new obituary</h1>
					<div class="in-co-breadcrumbs v2 pt1 pb1 text-center">
						<ul>
							<li>Home</li>
							<li>Post New Obituaries</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="container-fluid ob-temp-top animate">
				<div class="auto med text-center">
					<h2 class="hb-title med bp-tt pt5 pb5">Select a Template</h2>
                    <center>
                       <div class="obituray-template">
                        <div class="hard"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/page_template/book-cover.jpg" alt=""></div>
                        <div class="hard"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/page_template/book-cover-2.jpg" alt="" data-template="template-1"></div>
                        <div><img class="img-responsive" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/page_template/template-1.jpg" alt=""></div>
                        
                        <div><img class="img-responsive" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/page_template/template-3.jpg" alt="" data-template="template-3"></div>
                        <div><img class="img-responsive" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/page_template/template-4.jpg" alt="" data-template="template-4"></div>
                        <div><img class="img-responsive" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/page_template/template-2.jpg" alt="" data-template="template-2"></div>
                        <div class="hard"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/page_template/book-cover-3.jpg" alt=""></div>
                        <div class="hard"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/page_template/book-cover.jpg" alt=""></div>
                        </div>
                    </center>
				</div>
			</div>
			<div class="container-fluid pt5 pb5 animate">
				<div class="auto pt2 pb2">
					<div class="text-center par-max2 pb2">
						<h2 class="hb-title med bp-tt">Loved One’s Personal Information</h2>
						<p>Please provide the following information about your loved one :</p>
					</div>
					<div class="ob-co-form mt5 par-max2">
						<?php if ( is_active_sidebar( 'form_position_widget' ) ) : ?>
                            <?php dynamic_sidebar( 'form_position_widget' ); ?>
                        <?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php get_footer();
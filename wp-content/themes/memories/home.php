<?php get_header(); ?>
<header>
    <div class="header-content">
        <div class="auto med2"> 
            <div class="logo fl bp-img"><a href="index.html"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/page_template/icon_memories_of_life.png" alt=""></a></div>
            <div class="he-co-col right fr">
                <div class="hc-set menu">
                    <?php 
                        $args = array(
                            'theme_location' => 'primary',
                            'container'       => false,
                            'menu_class' => ''
                        );
                        wp_nav_menu( $args );
                    ?>
                </div>
                <!--div class="hc-set hc-ctrl">
                    <ul>
                        <li><a href="#"><span class="fi flaticon-camera"></span></a></li>
                        <li><a class="hc-set-search"><span class="fi flaticon-musica-searcher"></span></a></li>
                        <li><a href="#"><span class="fi flaticon-shopping-bag"></span></a></li>
                    </ul>
                </div-->
                <div class="hc-set menu-bar"><span></span><span></span><span></span></div>
            </div>
            <div class="clr"></div>
        </div>
    </div>
</header>

<!-- for floating menu -->
<div class="he-co-menu">
    <div class="auto med2 bp-rel">
        <div class="hc-me-close"><i class="fa fa-close"></i></div>
        <div class="hc-me-top">
            <div class="hc-ctrl">
                <ul>
                    <li><a href="#"><span class="fi flaticon-camera"></span></a></li>
                    <li><a href="#"><span class="fi flaticon-musica-searcher"></span></a></li>
                    <li><a href="#"><span class="fi flaticon-shopping-bag"></span></a></li>
                </ul>
            </div>
        </div>
        <div class="menu">
            <?php 
                $args = array(
                    'theme_location' => 'primary',
                    'container'       => false,
                    'menu_class' => ''
                );
                wp_nav_menu( $args );
            ?>
        </div>
    </div>
</div>
<!-- for floating menu -->
<div class="container-fluid he-co-floating hc-fl-search" style="display: none;">
    <div class="auto bp-rel">
        <div class="hc-me-close"><i class="fa fa-close"></i></div>
        <div class="hf-se-main" style="margin-top: 129px;">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input class="form-control" type="text" placeholder="Search...">
            </div>
        </div>
    </div>
</div>
<!-- Start of main content -->
<section id="main-wrapper">
    <div class="inner-content obituary-content pad-top">
        <div class="container-fluid obituary-paper pt5 pb5 animate">
            <div class="auto">
                <!-- for desktop -->
                <div class="bp-n-maxwidth">
                    <div id="obituary-book" class="pt2 pb2">
                        <div class="sample-docs">
                            <div class="hard"></div>
                            <div class="hard"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/obituary/book_page1.jpg" alt=""></div>
                            <div><a href="<?php echo get_site_url(); ?>/obituary"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/obituary/book_page2.jpg" alt=""></a></div>
                        </div>
                    </div>
                </div>

                <!-- for less than maxwidth -->
                <div class="bp-maxwidth">
                    <div class="bp-img wide mt2 mb2"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/obituary/book_page1.jpg" alt=""></div>
                    <div class="bp-img wide mt2 mb2"><a href="<?php echo get_site_url(); ?>/obituary"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/obituary/book_page2.jpg" alt=""></a></div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
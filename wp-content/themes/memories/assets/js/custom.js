//get all elements with class and get the biggest box
function get_biggest(elements){
	var biggest_height = 0;
	for ( var i = 0; i < elements.length ; i++ ){
		var element_height = $(elements[i]).outerHeight();
		//compare the height, if bigger, assign to variable
		if(element_height > biggest_height ) biggest_height = element_height;
	}
	return biggest_height;
}

function resize() {
	var windowWidth = $(window).width();
	var windowHeight = $(window).height();

	// STICKY FOOTER
	var headerHeight = $('header').outerHeight();
	var footerHeight = $('footer').outerHeight();
	var footerTop = (footerHeight) * -1
	$('footer').css({marginTop: footerTop});
	$('#main-wrapper').css({paddingBottom: footerHeight});
	$('.pad-top').css({paddingTop: headerHeight});

	// for vertically middle content
	$('.bp-middle').each(function() {
		var bpMiddleHeight = $(this).outerHeight() / 2 * - 1;
		$(this).css({marginTop: bpMiddleHeight});
	});

	// for home banner
	$('.home-banner').css({height: windowHeight});
	var hobaMain = ($('.home-banner').outerHeight() - $('.ho-ba-main').outerHeight()) / 2;
	if(hobaMain < 0) { 
		$('.ho-ba-main').css({marginTop: 0});
	} else {
		$('.ho-ba-main').css({marginTop: hobaMain});
	}

	// for home banner
	$('.hb-ma-tag').each(function() {
		hsTagWidth = $(this).outerWidth();
		hsTagLeft = ($(this).outerWidth() / 2) * -1;
		hsTagHeight = $(this).outerHeight();
		$(this).css({left: hsTagLeft, marginTop: hsTagHeight});
	});

	// for home product
	$('.hp-se-tag').each(function() {
		hsTagWidth = $(this).outerWidth();
		hsTagRight = ((hsTagWidth / 2) - 20) * -1;
		hsTagBottom = (hsTagWidth / 2) + 8;
		$(this).css({right: hsTagRight, bottom: hsTagBottom});
	});

	// for floating menu
	var hcMenuHeight = (windowHeight - $('.he-co-menu .menu').outerHeight()) / 2;
	var hcmetop = $('.hc-me-top').outerHeight();
	if(hcMenuHeight < hcmetop) {
		$('.he-co-menu div.menu').css({top: hcmetop});
	} else {
		$('.he-co-menu div.menu').css({top: hcMenuHeight});
	}

	// for floating search
	var hcSearchTop = headerHeight;
	$('.hf-se-main').css({marginTop: hcSearchTop});	

	// for equalizer
	$('.in-ob-set .main').css({minHeight: 0});
	var inobSet = get_biggest($('.in-ob-set .main'));
	$('.in-ob-set .main').css({minHeight: inobSet});
}

$(window).resize(function() {
	resize();
});

$(window).scroll(function() {
	bpOnscroll();
});

$(document).ready(function() {
	if (Modernizr.touch) {
		$('html').addClass('bp-touch');
	}

	// AUTO SCROLL
	$('.bp-click').click(function(e) { 
		var layer = $(this).attr('data-name'); 
		$('body, html').animate({scrollTop:$(layer).offset().top}, 1000); 
		event.preventDefault(); 
	}); 

	// for owl carousel
	$('.home-instagram .owl-carousel').owlCarousel({
		autoplay		: true,
		loop			: true,
		margin			: 0,
		nav				: true,
		navText			: ['',''],
		mouseDrag		: true,
		smartSpeed		: 1000,
		responsive:{
			0:{ items:2},
			400:{ items:3},
			800:{ items:4},
			1024:{ items: 6}
		}
	});

	// for footer toggle
	$('.ft-co-list li.set.sub .ft-title').click(function() {
		if($(window).width() < 810) {
			if($(this).hasClass('selected')) { 
				$(this).removeClass('selected');
				$(this).next().slideUp(200);
			} else {
				$('.ft-co-list .ft-title').removeClass('selected');
				$(this).addClass('selected');

				$('.ft-co-list li.set.sub .ft-title').next().slideUp(200);
				$(this).next().slideDown(200);
			}
		}
	});

	// for menu 
	$('header .menu li.sub > a').removeAttr('href');
//	$('header .menu li.sub').mouseenter(function() {
//		$('ul', this).delay(100).slideDown(200);
//	});
//	$('header .menu li.sub').mouseleave(function() {
//		$('ul', this).stop(true,true).slideUp(200);
//	});
    //Hercival MOdification for wordpress integration
    $('header .menu li.menu-item-has-children').mouseenter(function() {
        $('ul:first', this).delay(100).slideDown(200);
	});
    $('header .menu li.menu-item-has-children').mouseleave(function() {
		$('ul:first', this).stop(true,true).slideUp(200);
	});
//	$('.he-co-menu .menu ul').each(function() {
//		var thisNav = $(this);
//
//		$('> li.sub > a', thisNav).removeAttr('href');
//
//		$('> li.sub > a', thisNav).click(function() {
//			if($(this).hasClass('selected')) { 
//				$(this).removeClass('selected');
//				$(this).next().slideUp(200);
//			} else {
//				$('> li.sub > a', thisNav).removeClass('selected');
//				$('> li.sub > a', thisNav).next().slideUp(200);
//
//				$(this).addClass('selected');
//				$(this).next().slideDown(200);
//			}
//
//			setTimeout(function() {
//				resize();
//			}, 150)
//		});
//
//	});
    
    $('.he-co-menu .menu ul').each(function() {
		var thisNav = $(this);

		$('> li.menu-item-has-children > a', thisNav).removeAttr('href');

		$('> li.menu-item-has-children > a', thisNav).click(function() {
			if($(this).hasClass('selected')) { 
				$(this).removeClass('selected');
				$(this).next().slideUp(200);
			} else {
				$('> li.menu-item-has-children > a', thisNav).removeClass('selected');
				$('> li.menu-item-has-children > a', thisNav).next().slideUp(200);

				$(this).addClass('selected');
				$(this).next().slideDown(200);
			}

			setTimeout(function() {
				resize();
			}, 150)
		});

	});

	bpOnscroll();
	bpParallax();
	forMenu();
	forSearch();
	resize();
});

$(window).load(function() {
	resize();
});

// preloader once done
Pace.on('done', function() {
	// totally hide the preloader especially for IE
	setTimeout(function() {
		$('.pace-inactive').hide();
	}, 500);
	// for home banner
	$('.home-banner').addClass('active');
});

RippleAnim();
function RippleAnim() {
	if (Modernizr.touch) {
		$('.animate').removeClass('animate').removeAttr('anim-control').removeAttr('anim-delay').removeAttr('anim-name');
	}

	$('.animate').each(function() {
		var thisAnim = $(this); // this content
		var animControl = $(this).attr('anim-control'); // for single or parent container
		var animName = $(this).attr('anim-name'); // customize animation
		var animDelay = parseFloat($(this).attr('anim-delay')); // delay value

		// if the container is parent
		if(animControl == 'parent') {
			// add delay to each child element
			$(this).children().each(function(index) {
				var element = $(this);
				if(isNaN(animDelay)) {
					animDelay = 0.5;
				}
				var delayNum = animDelay + (0.2 * index) + 's';
				setTimeout(function() {
					$(element).css('-webkit-animation-delay', delayNum)
						.css('-moz-animation-delay', delayNum)
						.css('-ms-animation-delay', delayNum)
						.css('-o-animation-delay', delayNum)
				}, 100);
			});

			if(animName == null) {
				// if no customize animation then use default animation
				$(this).children().each(function(index) {
					$(this).addClass('anim-content');
				});
			} else {
				// if have customize animation
				$(this).children().each(function(index) {
					$(this).addClass('animated');
					$(this).on('inview',function(event,visible){
						if (visible == true) {
							$(this).addClass(animName);
						}
					});
				});
			}

		// if the container is not parent
		} else {
			if(animName == null) {
				// if no customize animation then use default animation
				$(this).addClass('anim-content');
			} else {
				// if have customize animation
				$(this).addClass('animated');
				$(this).on('inview',function(event,visible){
					if (visible == true) {
						$(this).addClass(animName);
					}
				});
			}
			// if have customize animation-delay
			if(animDelay != null) {
				if(isNaN(animDelay)) {
					animDelay = 0.5;
				}
				var delayNum = animDelay + 's';
				$(this).css('-webkit-animation-delay', delayNum)
						.css('-moz-animation-delay', delayNum)
						.css('-ms-animation-delay', delayNum)
						.css('-o-animation-delay', delayNum);
			}
		}

	});

	$.each($('.anim-content, .animated'),function(){
		$(this).on('inview',function(event,visible){
			if (visible == true) {
				$(this).addClass('visible');
			}
		});
	});
}

function bpParallax() {
	// for parallax effect
	$scene = $('#home-parallax');
	if($scene.length == 1) { 
		setTimeout(function() {
			$scene.parallax({
				frictionX: 0.06,
	  			frictionY: 0.06
			});
		}, 6600);
	}

	$scene2 = $('#memories-parallax');
	if($scene2.length == 1) { 
		$scene2.parallax({
			frictionX: 0.06,
  			frictionY: 0.06
		});
	}
}

function forMenu() {
	$('.header-content .menu-bar').click(function() {
		$('body').addClass('float-menu-active');
		$('.he-co-menu').fadeIn();
		$('.he-co-menu .hc-me-close').addClass('animated');

		resize();

		$('.he-co-menu').addClass('animated fadeInDown');
		setTimeout(function() {
			$('.he-co-menu').removeClass('animated fadeInDown');
			$('.he-co-menu .hc-me-close').addClass('zoomIn');
		}, 500);

	});

	$('.he-co-menu .hc-me-close').click(function() {
		closeMenu();
	});
}

function closeMenu() {
	$('body').removeClass('float-menu-active');
	$('.he-co-menu').addClass('animated slideOutUp');
	$('.he-co-menu .hc-me-close').removeClass('animated zoomIn');
	setTimeout(function() {
		$('.he-co-menu').removeClass('animated slideOutUp');
		$('.he-co-menu').hide();
	}, 500);
}

function forSearch() {
	$('.hc-set-search').click(function() {
		$('body').addClass('float-menu-active');
		$('.hc-fl-search').fadeIn();
		$('.hf-se-main input').focus();

		$('.hf-se-main').addClass('animated bounceInDown');
		$('.hc-fl-search .hc-me-close').addClass('animated zoomIn');
		setTimeout(function() {
			$('.hf-se-main').removeClass('animated bounceInDown');
			$('.hc-fl-search .hc-me-close').removeClass('animated zoomIn');
		}, 1000);

		closeMenu();
	});

	$('.hc-fl-search .hc-me-close').click(function() {
		$('body').removeClass('float-menu-active');

		$('.hf-se-main').addClass('animated bounceOutUp');
		$('.hc-fl-search .hc-me-close').addClass('animated zoomOut');
		setTimeout(function() {
			$('.hf-se-main').removeClass('animated bounceOutUp');
			$('.hc-fl-search .hc-me-close').removeClass('animated zoomOut');
		}, 1000);
		setTimeout(function() {
			$('.hc-fl-search').fadeOut();
		}, 500);
	});
}

bpFlipBook();
function bpFlipBook() {
	$flipbook = $('.sample-docs');

	if($flipbook.length == 1) {
		$flipbook.turn({
			width 		: 1200,
			height 		: 798,
			elevation 	: 50,
			acceleration: false,
			gradients 	: true,
			autoCenter 	: true,
			duration 	: 1000
		}).turn('page', 2);
	}
}

var headerUp = 0;
var headerDown = 0;
function bpOnscroll() {
 var headerHeight = $('header').outerHeight();
 	if($('header').hasClass('v2')) {
 		if($(window).scrollTop() > headerHeight){
			$('.header-content').removeClass('up');
			$('.header-content').addClass('down');
			HeaderControl();
		} else {
			$('.header-content').removeClass('down');
			$('.header-content').addClass('up');
			HeaderControl();
		}
 	}
}

function HeaderControl() {
	if($('.header-content').hasClass('down')) {
		if (headerDown == 0) {
			var logoHeight = $('.header-content .logo .v2').outerHeight();
			$('.header-content .logo').animate({height: logoHeight}, 200);
			$('.header-content .logo .v1').animate({opacity: 0}, 200);
			$('.header-content .logo .v2').animate({opacity: 1}, 200);

			headerUp = 0;
			headerDown = 1;
		}
	} else if ($('.header-content').hasClass('up')) {
		if (headerUp == 0) { 
			var logoHeight = $('.header-content .logo .v1').outerHeight();
			$('.header-content .logo').animate({height: logoHeight}, 200);			
			$('.header-content .logo .v1').animate({opacity: 1}, 200);
			$('.header-content .logo .v2').animate({opacity: 0}, 200);
			
			headerDown = 0;
			headerUp = 1;
		}
	}
	setTimeout(function() {
		resize();
	}, 100)
}

templateFlipBook();
function templateFlipBook() {
	$flipbook = $('.obituray-template');

	if($flipbook.length == 1) {
		$flipbook.turn({
			width 		: 904,
			height 		: 604,
			elevation 	: 10,
			autoCenter 	: true,
            page:2
		});
	}
}

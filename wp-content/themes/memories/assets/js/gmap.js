function initAutocomplete() {
    var map = new google.maps.Map(document.getElementById('map'), {
        center: {
            lat: 1.373272,
            lng: 103.894543
        },
        zoom: 13,
        mapTypeId: 'roadmap'
    });

    var input = document.getElementById('searchbox-gmap');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    map.addListener('bounds_changed', function () {
        searchBox.setBounds(map.getBounds());
    });

    var markers = [];
    searchBox.addListener('places_changed', function () {
        var places = searchBox.getPlaces();
        document.getElementById('gmap-place-id').value = places[0].place_id;
        if (places.length == 0) {
            return;
        }

        markers.forEach(function (marker) {
            marker.setMap(null);
        });
        markers = [];
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function (place) {
            if (!place.geometry) {
                console.log("Returned place contains no geometry");
                return;
            }
            var icon = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
            };
            markers.push(new google.maps.Marker({
                map: map,
                icon: icon,
                title: place.name,
                position: place.geometry.location
            }));

            if (place.geometry.viewport) {
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
        });
        map.fitBounds(bounds);
    });
}
initAutocomplete();

jQuery(document).ready(function($) {
    $( ".datepicker" ).datepicker({dateFormat : 'dd/mm/yy',changeMonth: true,changeYear: true});
    $('#new-ob-form').bootstrapValidator({
        live: 'enabled',
        fields: {
            fname: {
                validators: {
                        stringLength: {
                        min: 2,
                    },
                        notEmpty: {
                        message: 'Please supply First Name'
                    }
                }
            },
             mname: {
                validators: {
                     stringLength: {
                        min: 1,
                    },
                    notEmpty: {
                        message: 'Please supply Middle Name'
                    }
                }
            },

            lname: {
                validators: {
                    
                    notEmpty: {
                        message: 'Please supply Last Name'
                    }
                }
            },
            designation: {
                validators: {
                     stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please supply your designation'
                    }
                }
            },
            track: {
                validators: {
                    notEmpty: {
                        message: 'Please Select Your Track'
                    }
                }
            }
        }
    });
    $('#obituary-photo-input').change(function(){
        imagePreview(this);
    });
    function imagePreview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image-preview')
                    .attr('src', e.target.result)
                    //.width(150)
                    .height(180);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    
    //template selector
    $('.obituray-template .page-wrapper img').click(function(){
        $('.obituray-template .page-wrapper img').css('opacity','1').css('border' ,'none');
        $(this).css('opacity','0.5').css('border' ,'solid 10px #00b0c5');
        $('.obituray-template .page-wrapper').css('background-color','#000');
        var template = $(this).data('template');
        $('#template_id').val(template);
    });

});

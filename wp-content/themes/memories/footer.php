</section>
    <footer>
        <div class="footer-content">
            <div class="auto">
                <div class="footer-top pad-edge">
                    <div class="auto med pt5 pb5 bp-rel">
                        <div class="ft-co-list pb1">
                            <ul>
                                <li class="set sub">
                                    <h4 class="ft-title pb3">contact</h4>
                                    <div class="ft-main par2">
                                        <p>
                                            Phone : +65 6334 7448 <br>
                                            HP : +65 9782 0262 <br>
                                            Fax : +65 9782 0262
                                        </p>
                                        <p>
                                            2 Bukit Batok 24 , #01-14 <br>
                                            Skytech Building, <br>
                                            Singapore 659480
                                        </p>
                                        <p><a href="mailto:darren@cckmarble.com.sg">darren@cckmarble.com.sg</a></p>

                                        <div class="ft-sns pt1">
                                            <ul>
                                                <li>
                                                    <a class="sns-fb" href="https://www.facebook.com/memoriesoflifebycckmarble/" target="_blank">
                                                        <i class="fa fa-facebook"></i>
                                                    </a>
                                                </li>
                                                <li><a class="sns-g-plus" href="http://www.facebook.com/" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                                                <li><a class="sns-instagram" href="http://www.facebook.com/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                                <li><a href="http://www.facebook.com/" target="_blank"><i class="fa fa-phone"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                <li class="set sub">
                                    <h4 class="ft-title pb3">informations</h4>
                                    <div class="ft-main par2">
                                        <div class="ft-links">
                                           <?php 
                                                $args = array(
                                                    'theme_location' => 'footer',
                                                    'container'       => false,
                                                    'menu_class' => ''
                                                );
                                                wp_nav_menu( $args );
                                            ?>
                                        </div>
                                    </div>
                                </li>
                                <li class="set large">
                                    <h4 class="ft-title pb3">newsletters</h4>
                                    <div class="ft-newsletter">
                                        <form action="" method="post">
                                            <div class="form-group">
                                                <input class="form-control" type="email" name="" value="" placeholder="E-mail Address">
                                            </div>
                                        </form>
                                    </div>
                                    <div class="ft-frame pad-edge mt2">
                                        <div class="bp-img"><div class="fb-page" data-href="https://www.facebook.com/memoriesoflifebycckmarble/" data-width="500" data-small-header="false" data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/memoriesoflifebycckmarble/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/memoriesoflifebycckmarble/">Memories Of Life by CCK Marble</a></blockquote></div></div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="ft-back"><a class="bp-click" data-name="#main-container"><span class="fi flaticon-up-arrow"></span></a></div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom pad-edge">
                <div class="auto pt2 pb4 bp-center">
                    <div class="fb-logo bp-set"><a href="index.html"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/page_template/icon_memories_of_life.png" alt=""></a></div>
                    <div class="fb-copyright">&copy; 2016 memories of life  ·  ALL RIGHTS RESERVED.</div>
                    <div class="clr"></div>
                </div>
            </div>
        </div>
    </footer>
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=164061293727877";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/lib/jquery.min.js"><\/script>')</script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/lib/jquery-ui.min.js"><\/script>')</script>

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="js/bootstrap.min.js"><\/script>')</script>
        <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/js/plugins.js"></script>
        <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/js/jquery.inview.min.js"></script>
        <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/js/owl.carousel.min.js"></script>
        <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/js/parallax.js"></script>

        <!-- for flipbook -->
        <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/js/turn.min.js"></script>
        <!-- for flipbook -->
        <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/js/custom.js"></script>
       <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC_yJ9-3IJsfDaxaBGxmPI7lJWPpWoHMZw&libraries=places"></script>
        
        <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/js/validator.js"></script>
        <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/js/gmap.js"></script>
        
    </body>
</html>
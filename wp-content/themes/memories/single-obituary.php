<?php get_header(); ?>
<header>
    <div class="header-content">
        <div class="auto med2"> 
            <div class="logo fl bp-img"><a href="index.html"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/page_template/icon_memories_of_life.png" alt=""></a></div>
            <div class="he-co-col right fr">
                <div class="hc-set menu">
                    <?php 
                        $args = array(
                            'theme_location' => 'primary',
                            'container'       => false,
                            'menu_class' => ''
                        );
                        wp_nav_menu( $args );
                    ?>
                </div>
                <!--div class="hc-set hc-ctrl">
                    <ul>
                        <li><a href="#"><span class="fi flaticon-camera"></span></a></li>
                        <li><a class="hc-set-search"><span class="fi flaticon-musica-searcher"></span></a></li>
                        <li><a href="#"><span class="fi flaticon-shopping-bag"></span></a></li>
                    </ul>
                </div-->
                <div class="hc-set menu-bar"><span></span><span></span><span></span></div>
            </div>
            <div class="clr"></div>
        </div>
    </div>
</header>

<!-- for floating menu -->
<div class="he-co-menu">
    <div class="auto med2 bp-rel">
        <div class="hc-me-close"><i class="fa fa-close"></i></div>
        <div class="hc-me-top">
            <div class="hc-ctrl">
                <ul>
                    <li><a href="#"><span class="fi flaticon-camera"></span></a></li>
                    <li><a href="#"><span class="fi flaticon-musica-searcher"></span></a></li>
                    <li><a href="#"><span class="fi flaticon-shopping-bag"></span></a></li>
                </ul>
            </div>
        </div>
        <div class="menu">
            <?php 
                $args = array(
                    'theme_location' => 'primary',
                    'container'       => false,
                    'menu_class' => ''
                );
                wp_nav_menu( $args );
            ?>
        </div>
    </div>
</div>
<!-- for floating menu -->
<div class="container-fluid he-co-floating hc-fl-search" style="display: none;">
    <div class="auto bp-rel">
        <div class="hc-me-close"><i class="fa fa-close"></i></div>
        <div class="hf-se-main" style="margin-top: 129px;">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input class="form-control" type="text" placeholder="Search...">
            </div>
        </div>
    </div>
</div>
<!-- Start of main content -->
while ( have_posts() ) : the_post();
    $meta = get_post_meta( get_the_ID() );?>
    <section id="main-wrapper">
        <div class="inner-content obituary-content pad-top animate">
            <div id="memories-parallax" class="ob-co-top">
                <div class="container-fluid oc-to-top v2 pt4 pb4">
                    <div class="oc-to-main text-center">
                        <div class="in-co-breadcrumbs v2 pt1 pb1 text-center">
                            <ul>
                                <li>Home</li>
                                <li>Obituaries</li>
                                <li><?php echo ucwords(strtolower(get_the_title()));?></li>
                            </ul>
                        </div>
                        <h1 class="hb-title text-center bp-tt"><?php the_title();?></h1>
                        <div class="oc-to-box par-large bp-set mt2 mb2">
                            <div class="oc-to-img bp-img wide mb1">
                                <?php echo the_post_thumbnail( 'thumbnail' );?>
                            </div>
                            <p><strong><i>Beloved <?php echo $meta['relationship'][0];?></i></strong></p>
                            <p class="co-blue-sea"><strong><i><?php echo date('d.m.Y',$meta['orbi_from_date'][0]);?> – <?php echo date('d.m.Y',$meta['orbi_to_date'][0]);?></i></strong></p>
                        </div>
                        <div class="clr"></div>
                        <div class="oc-to-pad pb4"></div>
                    </div>
                    <div class="oc-to-item clouds one layer" data-depth="0.12"></div>
                    <div class="oc-to-item clouds two layer" data-depth="0.18"></div>
                    <div class="oc-to-item dove layer" data-depth="0.1"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/obituary/bg_dove.png" alt=""></div>
                </div>
                <div class="container-fluid ob-co-bottom pb5">
                    <div class="auto" style="max-width: 890px;">
                        <div class="ot-bo-box bp-center par-xlarge par">
                            <div class="ot-bo-flower bp-img"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/obituary/flower_wreath.png" alt=""></div>
                            <div class="co-blue-sea">
                                <p><i><?php echo ucwords(strtolower(get_the_title()));?>, a devoted mother, sister, grandmother, aunt and friend to so many passed away peacefully at her home in funeral caring home on Tuesday, March 19, 2013. Elishabeth Rowe was born on September 18, 1948 in Newyork.</i></p>
                            </div>
                            <div class="bp-img text-center mt1 mb1"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/obituary/icon_o.png" alt=""></div>
                            <div class="co-gray">
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid ob-co-ctrl text-center">
                    <ul>
                        <li><a class="btn ob-btn blue bp-set vm" href="#"><i class="fi flaticon-bouquet"></i> <span>send flowers</span></a></li>
                        <!--li><a class="btn ob-btn bp-set vm" href="#"><i class="fa fa-share-alt"></i> <span>share</span></a></li-->
                    </ul>
                </div>
            </div>
            <div class="container-fluid pt5 pb5">
                <div class="auto med2 pt3 pb3">
                    <h2 class="hb-title small text-center bp-tt">Wake & Funeral Details</h2>
                    <div class="row ob-co-det mt2 par-large fa-light">
                        <div class="col-sm-5 mt3">
                            <h4>Help inform a friend</h4>
                            <div class="ob-sns mt1 mb1">
                                <ul>
                                    <li><a class="sns-whatsapp" href="#"><i class="fa fa-whatsapp"></i></a></li>
                                    <li><a class="sns-fb" href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a class="sns-twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a class="sns-g-plus" href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a class="sns-linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                            <div class="mt4">
                                <h4>Wake</h4>
                                <p>The late Mdm. Elishasbeth Rowe is resting peacefully at <br>
                                    <span class="co-blue-sea fa-sem"><?php echo $meta['wake_address'][0];?></span>
                                </p>
                            </div>
                            <div class="mt4">
                                <h4>Funeral Details</h4>
                                <p><?php echo $meta['funeral_details'][0];?></p>
<!--
                                <p>
                                    The Cortege will leave on <br>
                                    <span class="fa-sem"><u>Wednesday, 14 Dec 2016, 10:45 AM,</u></span> <br>
                                    from Blk 460 Hougang Ave 10, <br>
                                    Cremation will be held at Mandai Crematorium Hall 4 at <br>
                                    <span class="fa-sem"><u>11:45 AM.</u></span>
                                </p>
                                <p>For more information, kindly contact the family members.</p>
-->
                            </div>
                        </div>
                        <div class="col-sm-4 mt3">
                            <h4>location</h4>
                            <div class="ob-co-frame">
                                <iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyBWUNN3is8SadS7-NAzyUMAqL7lw_FFX6A&q=place_id:<?php echo $meta['gmap_place_id'][0];?>" width="100%" height="290" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="col-sm-3 mt3">
                           <?php funeral_service_html($meta['funeral_service'][0]);?>
                        </div>
                    </div>
                </div>
                <div class="bp-img bp-leaves text-center"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/home/flower_divider_bottom.png" alt=""></div>
                <?php comments_template('/alternative-comments.php'); ?> 

            </div>
        </div>
    </section>
 <?php endwhile; ?>
 <?php get_footer();?>
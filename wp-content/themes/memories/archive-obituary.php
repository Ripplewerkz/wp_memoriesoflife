<?php 
/**
 * Template Name: Obituary Archives
 * Description: Used as a page template to show page contents, followed by a loop through a Obituary Custom Post archive  
 */
get_header(); ?>
<header>
    <div class="header-content">
        <div class="auto med2"> 
            <div class="logo fl bp-img"><a href="index.html"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/page_template/icon_memories_of_life.png" alt=""></a></div>
            <div class="he-co-col right fr">
                <div class="hc-set menu">
                    <?php 
                        $args = array(
                            'theme_location' => 'primary',
                            'container'       => false,
                            'menu_class' => ''
                        );
                        wp_nav_menu( $args );
                    ?>
                </div>
                <!--div class="hc-set hc-ctrl">
                    <ul>
                        <li><a href="#"><span class="fi flaticon-camera"></span></a></li>
                        <li><a class="hc-set-search"><span class="fi flaticon-musica-searcher"></span></a></li>
                        <li><a href="#"><span class="fi flaticon-shopping-bag"></span></a></li>
                    </ul>
                </div-->
                <div class="hc-set menu-bar"><span></span><span></span><span></span></div>
            </div>
            <div class="clr"></div>
        </div>
    </div>
</header>

<!-- for floating menu -->
<div class="he-co-menu">
    <div class="auto med2 bp-rel">
        <div class="hc-me-close"><i class="fa fa-close"></i></div>
        <div class="hc-me-top">
            <div class="hc-ctrl">
                <ul>
                    <li><a href="#"><span class="fi flaticon-camera"></span></a></li>
                    <li><a href="#"><span class="fi flaticon-musica-searcher"></span></a></li>
                    <li><a href="#"><span class="fi flaticon-shopping-bag"></span></a></li>
                </ul>
            </div>
        </div>
        <div class="menu">
            <?php 
                $args = array(
                    'theme_location' => 'primary',
                    'container'       => false,
                    'menu_class' => ''
                );
                wp_nav_menu( $args );
            ?>
        </div>
    </div>
</div>
<!-- for floating menu -->
<div class="container-fluid he-co-floating hc-fl-search" style="display: none;">
    <div class="auto bp-rel">
        <div class="hc-me-close"><i class="fa fa-close"></i></div>
        <div class="hf-se-main" style="margin-top: 129px;">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input class="form-control" type="text" placeholder="Search...">
            </div>
        </div>
    </div>
</div>
<!-- Start of main content -->
<section id="main-wrapper">
		<div class="in-fl-co right">
			<a class="bp-img" href="<?php echo get_site_url(); ?>/post-new-obituary"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/obituary/btn_post_new.png" alt=""></a>
		</div>
		<div class="inner-content obituary-content pad-top">
			<div class="container-fluid pt5 pb5 animate">
				<div class="auto pt2 pb2">
					<h1 class="hb-title text-center bp-tt">OBITUARY</h1>
					<div class="in-co-breadcrumbs v2 pt1 pb1 text-center">
						<ul>
							<li>Home</li>
							<li>Obituaries</li>
						</ul>
					</div>
					<div class="in-ob-list col3 mt2 mb2 par-large co-gray">
						<ul class="animate" anim-control="parent" anim-delay="1">
                            <?php
                                while ( have_posts() ) : the_post();
                                    $meta = get_post_meta( get_the_ID() );?>
                                    <li class="set">
                                        <div class="in-ob-set">
                                            <div class="main text-center">
                                                <div class="in-ob-img bp-img bp-set">
                                                    <a href="<?php the_permalink(); ?>">
                                                        <?php echo the_post_thumbnail( 'thumbnail' );?>
                                                    </a>
                                                </div>
                                                <div class="clr"></div>
                                                <h3><?php the_title();?></h3>
                                                <p class="co-ash">(Age <?php echo $meta['age_died'][0];?>)</p>
                                                <p><strong><i>Beloved <?php echo $meta['relationship'][0];?></i></strong></p>
                                                <p class="co-blue-sea">
                                                    <strong>
                                                        <i><?php echo date('d.m.Y',$meta['orbi_from_date'][0]);?> –  <?php echo date('d.m.Y',$meta['orbi_to_date'][0]);?></i>
                                                    </strong>
                                                </p>
                                                <div class="ho-co-par">
                                                    <p><?php the_excerpt(); ?></p>
                                                </div>
                                                <p class="pt2"><strong><a href="<?php the_permalink(); ?>"><i>Read More</i></a></strong></p>
                                            </div>
                                        </div>
                                    </li>
                            <?php endwhile; ?>
						</ul>
					</div>
					<div class="text-center par-large mt4">
						<?php posts_nav_link(); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>

<?php get_footer(); ?>
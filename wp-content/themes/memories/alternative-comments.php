<div class="ob-co-col mt5 mb5">
    <div class="auto med">
        <h2 class="hb-title small text-center bp-tt pb3">Condolences & Messages</h2>
            <?php if($comments) : ?>
                <?php foreach($comments as $comment) : ?>
                    <?php if ($comment->comment_approved == '1') : ?>
                    <div class="oc-co-box par-medium fa-light">
                        <div class="oc-bo-media mb2">
                            <span class="user"><div class="oc-bo-user"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/obituary/user_default2.jpg" alt=""></div></span>
                            <span class="large">
                                <h4>A MESSAGE FROM <?php echo strtoupper($comment->comment_author);?></h4>
                            </span>
                        </div>
                        <p><?php echo $comment->comment_content; ?></p>
                        <div class="co-ash mt2">
                            <span><i><?php echo ucwords($comment->comment_author);?>, <?php echo date('F d, Y', strtotime($comment->comment_date))?></i></span>
                        </div>
                    </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php else : ?>
            <?php endif; ?>
    </div>
</div>
<?php if(comments_open()) : ?>
<div class="ob-co-col mt5 mb5">
    <div class="auto med">

        <h2 class="hb-title small text-center bp-tt pb3">leave a Condolences & Messages</h2>
        <div class="oc-co-box par-medium fa-light">
            <form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post">
                <div class="oc-bo-media mb2">
                    <span class="user vt"><div class="oc-bo-user v2"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/obituary/user_default.jpg" alt=""></div></span>
                    <span class="large vt">
                        <div class="form-group">
                            <input type="text" class="form-control" name="author" id="author" value="" placeholder="Name"/>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="email" id="email" value="" placeholder="Email"/>
                        </div>
                        <textarea class="form-control" name="comment" id="comment" cols="0" rows="0" placeholder="Write a condolence."></textarea>	
                    </span>
                </div>
                <div class="text-right">
                    <input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" />
                    <input type="hidden" name="redirect_to" value="<?php echo get_permalink( $id);?>"/>
                    <input type="hidden" name="comment_status" value="0">
                    <button class="btn oc-btn par-large" type="submit">submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php else : ?>
    <div class="ob-co-col mt5 mb5">
        <div class="auto med">
            <h2 class="hb-title small text-center bp-tt pb3">The comments are closed.</h2>
        </div>
    </div>
<?php endif; ?>            

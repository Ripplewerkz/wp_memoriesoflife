<?php

function memories_setup() {
	load_theme_textdomain( 'memories' );

	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );

	add_theme_support( 'post-thumbnails' );
	add_image_size( 'memories-featured-image', 2000, 1200, true );
	add_image_size( 'memories-thumbnail-avatar', 179, 180, true );
    
    //navigation menus
    register_nav_menus( array( 
        'primary' => __('Main Menu'),
        'petmenu' => __('Pet Menu'),
        'footer' => __('Inforamtion Menu'),
        )
    );
}
add_action( 'after_setup_theme', 'memories_setup' );
add_filter('show_admin_bar', '__return_false');

function memories_custom_excerpt_length( $length ) {
    return 25;
}
function memories_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'memories_excerpt_more' );
add_filter( 'excerpt_length', 'memories_custom_excerpt_length', 999 );

function memories_resources(){
    wp_enqueue_style('memories-boostrap-style', get_template_directory_uri(). '/assets/css/bootstrap.min.css');
    wp_enqueue_style('memories-boostrap-theme', get_template_directory_uri(). '/assets/css/bootstrap-theme.min.css');
    wp_enqueue_style('memories-main-style', get_template_directory_uri(). '/assets/css/style.css');
    wp_enqueue_style('memories-main-animate', get_template_directory_uri(). '/assets/css/animate.css');
    wp_enqueue_style('memories-validation-template', get_template_directory_uri(). '/assets/css/template.css');
    wp_enqueue_style('memories-validation-engine', get_template_directory_uri(). '/assets/css/validationEngine.jquery.css');
    
    wp_enqueue_script( 'memories-js-html5', get_template_directory_uri() . '/assets/js/html5.js');
    wp_enqueue_script( 'memories-js-modernizr', get_template_directory_uri() . '/assets/js/lib/modernizr-2.8.3.min.js');
    wp_enqueue_script( 'memories-js-pace', get_template_directory_uri() . '/assets/js/pace.min.js');

}
add_action('wp_enqueue_scripts', 'memories_resources');
add_filter('nav_menu_css_class' , 'active_filter_nav_class' , 10 , 2);

function active_filter_nav_class ($classes, $item) {
    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'active ';
    }
    return $classes;
}
add_action( 'pre_get_posts', function ( $query ) {

    if( !is_admin() && $query->is_main_query() && $query->is_post_type_archive( 'obituary' ) ) {

        $query->set( 'posts_per_page', 6 );

    }

});

function memories_widget() {
 register_sidebar(array(
 'name' => __( 'Memory of Life Widget Form', 'memories' ), 
 'id' => 'form_position_widget',
 'description' => __( 'The position of New Obituary Form', 'memories' ),
 ));
}
add_action( 'widgets_init', 'memories_widget' );

function pet_memories_widget() {
 register_sidebar(array(
 'name' => __( 'Memory of Life Widget Pet Memorial Form', 'memories' ), 
 'id' => 'pet_form_position_widget',
 'description' => __( 'The position of New Pet Memorial Form', 'memories' ),
 ));
}
add_action( 'widgets_init', 'pet_memories_widget' );


function funeral_service_html($funeral_service_id){
    
    $args = array(
        'p'   => $funeral_service_id,
        'post_type'   => 'funeralservice',
        'post_status' => 'publish'
    );
    $returnhtml = '';
    $funeralservice = new WP_Query( $args );
    if( $funeralservice->have_posts() ) :
        while( $funeralservice->have_posts() ) :
            $funeralservice->the_post();
                echo '<h4>FUNERAL SERVICE</h4>';
                echo '<div class="mt5 mb5 bp-img">';
                        the_post_thumbnail($funeral_service_id,'thumbnail');
                echo '</div>';
                echo '<p class="fa-sem">'.get_the_title().'</p>'.get_the_content();
        endwhile;
        wp_reset_postdata();
        
    endif;
 }




<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'memoryoflife');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '?p5b`9Fwk}$X_5KJ7fpbOC,f}UNlYh2`h9zmC=(mTfEWX;FcjaE2(Uei[)SBvNmH');
define('SECURE_AUTH_KEY',  '+6O;a5}|E$$.j@=KP!W8[DFpogK^a0h:1(m-KB{Vl`:Q?8nl$)!3y!gL3+^zGeD1');
define('LOGGED_IN_KEY',    'PIU}QQfU;2E!q-5uB*oaV|7^z;`S>eSjr^=QW(1^#nNQSc^}Vi:Dx>b~rsY8A:R0');
define('NONCE_KEY',        '6iW(xCj`mMGSH]y/<K-HlEwnqcfdC$T{|t=E1GRFQeyudnmB[@/j-BOn%S:Y:EUE');
define('AUTH_SALT',        '= c|AxEMF{8Ya/Q$]L^o?v}vEaWg/dO}yNigk:G!Diqd?k?7CjaP scW?_@pyv9;');
define('SECURE_AUTH_SALT', '[47}`;Qc`pD @FON3nXZ^8neuM*LeUa51 ua^.%UoC?$nngC1LoT1YEr*Ao+AV2,');
define('LOGGED_IN_SALT',   'DfW3]oK>TGf=Hkm:/e##^Sl+@?zKUgHVoX?Ofa-}B!@dJ<&(O1epfC( JUNeV%Zb');
define('NONCE_SALT',       '5*ZRH)w]BUo(lRsK{{t+pu[1Yq={P8i<oD,,[9_1|yBw5-VenyoQ+j?6S[nBlQU0');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
